# pbsimply-wpimporter

Import exported WordPress articles for site built with PureBuilder Simply.

# Depends

* Ruby \>2.0
* ActiveSupport

# Usage

## Summery

* Export *Articles* from WordPress site.
* Copy `wp-content` directory to destination.
* Copy `pbsimply-wpimporter.rb` to your working space (with WordPress exported xml.)
* Write `wpimporter.yaml` configuration file.
* Generate `base.html` on your working space.
* Write server config.

## wpimporter.yaml

|Key|Description|
|----------|--------------------------------|
|`outdir`|Base directory to output|
|`sourcefile`|Path to WordPress XML file|
|`pathprefix`|Replace internal domains `https?://[^/]+/` with this prefix instead of `/`.|

## Base HTML File

You can generate with Pandoc with the site template (or PureBuilder Simply) and source document like this:

```markdown
---
title: PUREBUILDER_CONV_TITLE
date: 1970-01-01
---

PUREBUILDER\_CONV\_BODY
```

`PUREBUILDER_CONV_TITLE` is replaced by title, `1970-01-01` is by timestamp (xmlschema) and include `PUREBUILDER_CONV_BODY` line is by article body.

And if line including `PUREBUILDER_CONV_TOC` is exist, it is replaced by auto generated TOC.

## Server config

For example, if you use Nginx and your wordpress path prefix is started with `archive/`, then you can

```
location /archive {
  default_type "text/html"
  root /srv/http/exported-wp;
}
```

or if you use Apache, you can use `.htaccess`:

```
<FilesMatch "^[^.]+$">
    ForceType text/html
</FilesMatch>
```

