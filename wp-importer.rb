require 'yaml'
require 'active_support/core_ext/hash'
require 'fileutils'
require 'date'
require 'erb'

class TOCBuilder
  def initialize(doc)
    @level = 1
    @toc_html = nil
    @toc_stack = (currentstack = [])
    @stack_stack = []
    
    doc.scan(%r:\<h([1-6]) [^>]*id="(.*?)"\>(.*?)\</h\1:im) do |level, id, text|
      level = level.to_i
      while level > @level
        @stack_stack.push(newstack = [])
        currentstack.push(newstack)
        currentstack = newstack
        @level += 1
      end
      while level < @level
        @stack_stack.pop
        currentstack = @stack_stack[-1]
        @level -= 1
      end
      currentstack.push({id: id, text: text})
    end
    return nil unless @toc_stack[0]
    [@toc_stack].each {|i| squash_toc i}

    @toc_html = convert_html(@toc_stack).join("\n")
  end

  attr :toc_html

  def squash_toc(tocarray)
    while tocarray.length == 1 && Array === tocarray[0]
      tocarray.replace(tocarray[0])
    end
    tocarray.each do |i|
      next unless Array === i
      squash_toc(i)
    end
  end

  def convert_html(array)
    items = array.map do |i|
      if Array === i
        convert_html(i)
      else
        sprintf('<li><a href="#%s">%s</a></li>', i[:id], i[:text])
      end
    end
    ["<ul>", items, "</ul>"]
  end

end

config = YAML.load File.read("wp-importer.yaml")
base_html = File.read("base.html")

articles = Hash.from_xml(File.read(config["sourcefile"]))

config["pathprefix"] ||= "/"

articles["rss"]["channel"]["item"].each do |article|

  # Skip if not public.
  next unless article["status"] == "publish"

  html = base_html.clone
  article_body = article["encoded"].join rescue next
  
  article_type = case article_body
  when /\<p\>/
    :HTML
  when /\<[a-z]+[^>]*\>/
    :partHTML
  else
    :plain
  end
  
  # Convert to HTML
  case article_type
  when :partHTML
    body_string = article_body.split(/(<\/?[A-Za-z0-9]+[^<]*>)/)
    tag_level = 0
    article_body = []
    body_string.each do |bodypart|
      case
      when bodypart[0, 2] == "</"
        tag_level -= 1
        article_body.push bodypart
      when /<[A-Za-z0-9]+[^>]*\/>/ === bodypart
        article_body.push bodypart
      when bodypart[0] == "<"
        tag_level += 1
        article_body.push bodypart
      when tag_level > 0
        article_body.push bodypart
      else
        article_body.push bodypart.split(/\n{2,}/).map {|i| "<p>#{i}</p>" }.join("\n")
      end
    end
    article_body = article_body.join
  when :plain
    article_body = article_body.split(/\n{2,}/).map {|i| "<p>#{i}</p>" }.join("\n")
  end

  # Convert Caption
  article_body.gsub!(%r!\[caption[^\[]*\]\s*(\<.*\>)(.*)\[/caption\]!) do |i|
    img = $1
    caption = $2
    img.gsub!(%r!https?://[^/]+/!, config["pathprefix"])
    sprintf('<figure>%s<figcaption>%s</figcaption></figure>', img, caption)
  end
  # And no caption images
  article_body.gsub!(%r!\<a[^>]+\><img .*?/></a>!) do |i|
    i.gsub(%r!https?://[^/]+/!, config["pathprefix"])
  end

  # Constructing TOC
  toc = TOCBuilder.new(article_body)
  if toc.toc_html
    html.gsub!(/.*PUREBUILDER_CONV_TOC.*/) { toc.toc_html }
  end

  
  outpath = File.join(config["outdir"], article["link"].sub(%r!https?://[^/]+/!, config["pathprefix"]))
  outdir = File.dirname(outpath)
  
  timestamp = Date.rfc822 article["pubDate"]
  html.gsub!("1970-01-01", timestamp.xmlschema)
  html.gsub!("PUREBUILDER_CONV_TITLE", article["title"].to_s)
  html.gsub!(/.*PUREBUILDER_CONV_BODY.*/) { article_body }
  
  unless File.exist? outdir
    FileUtils.mkdir_p outdir
  end
  File.open(outpath, "w") {|f| f.puts html}
end
